use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
//use serde_json::json;

#[derive(Deserialize)]
struct Request {
    command: String,
    data: Vec<f64>, // Example: Expecting a list of numbers for processing
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    processed_data: Vec<f64>, // The transformed data
    msg: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

    let func = service_fn(my_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

pub(crate) async fn my_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Perform some data processing. In this example, we'll simply double each number.
    let processed_data = event.payload.data.iter().map(|num| num * 2.0).collect::<Vec<_>>();

    // Prepare the response with the processed data
    let resp = Response {
        req_id: event.context.request_id,
        processed_data,
        msg: format!("Processed command: {}", event.payload.command),
    };

    Ok(resp)
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::{Context, LambdaEvent};

    #[tokio::test]
    async fn response_is_good_for_simple_input() {
        let id = "ID";

        let context = Context::default().with_request_id(id.to_string());

        let payload = Request {
            command: "process_data".to_string(),
            data: vec![1.0, 2.0, 3.0],
        };
        let event = LambdaEvent { payload, context };

        let result = my_handler(event).await.unwrap();

        assert_eq!(result.msg, "Processed command: process_data");
        assert_eq!(result.processed_data, vec![2.0, 4.0, 6.0]);
        assert_eq!(result.req_id, id.to_string());
    }
}
