# Define the base image from the Rust official Docker images
FROM rust:1.58 as builder

# Create a new empty shell project
RUN USER=root cargo new --bin lambda
WORKDIR /lambda

# Copy the source and the Cargo manifest files
COPY ./src ./src
COPY Cargo.toml Cargo.lock ./

# Build the application for the x86_64-unknown-linux-musl target
RUN rustup target add x86_64-unknown-linux-musl
RUN cargo build --release --target x86_64-unknown-linux-musl

# Use the Amazon Linux 2 base image provided by AWS
FROM public.ecr.aws/lambda/provided:al2

# Copy the compiled release binary from the builder stage
COPY --from=builder /lambda/target/x86_64-unknown-linux-musl/release/my_lambda_function /var/task/bootstrap

# The CMD is not necessary as AWS will call the bootstrap file directly
# CMD ["/var/task/bootstrap"]
