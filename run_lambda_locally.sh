#!/bin/bash

# Set AWS Lambda environment variables for local testing
export AWS_LAMBDA_FUNCTION_NAME=my_lambda_function
export AWS_LAMBDA_FUNCTION_VERSION=\$LATEST
export AWS_LAMBDA_FUNCTION_MEMORY_SIZE=128
export AWS_LAMBDA_LOG_GROUP_NAME=/aws/lambda/my_lambda_function
export AWS_LAMBDA_LOG_STREAM_NAME=$(date +%Y/%m/%d)/[$LATEST]$(uuidgen)
export AWS_REGION=us-east-1
export AWS_DEFAULT_REGION=us-east-1
export AWS_LAMBDA_RUNTIME_API=127.0.0.1:9001 # Placeholder value for local testingS
# Set these only if your Lambda function interacts with other AWS services
# export AWS_ACCESS_KEY_ID=your_access_key
# export AWS_SECRET_ACCESS_KEY=your_secret_key
# export AWS_SESSION_TOKEN=your_session_token

# Run the Lambda function with input from a file
cargo run < test/test_event.json
