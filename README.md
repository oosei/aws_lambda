# AWS Rust Lambda Function

This AWS Lambda function processes incoming JSON data, applies a transformation to the data, and returns the result. It's designed to work with AWS API Gateway, allowing it to be invoked via HTTP requests.

## Prerequisites

- Rust installed.
- AWS CLI configured with appropriate permissions.
- An AWS account and a basic understanding of AWS Lambda and API Gateway.

## Functionality

- This function expects a JSON payload with a command and data field. It processes the data based on the specified command and returns the processed data along with a message.

## Deployment

1. Compile the Function

- Ensure the Rust toolchain is set up for cross-compilation:

  `rustup target add x86_64-unknown-linux-musl`

- Compile the function for the AWS Lambda execution environment:

  `cargo lambda build --release --target x86_64-unknown-linux-musl`

2. Package the binary into a ZIP file.

- Navigate to the target directory and package the binary:

  `cp ./target/x86_64-unknown-linux-musl/release/my_lambda_function ./bootstrap`
  `zip -j bootstrap.zip ./bootstrap`

3. Deploy to AWS Lambda

- Create or update the Lambda function with the AWS CLI:

  `aws lambda update-function-code --function-name my_lambda_function --zip-file fileb://bootstrap.zip`

  ![deploy](img/deploy.png)

4. Set up an API Gateway trigger to invoke the function.
   Navigate to the AWS Management Console to set up an API Gateway trigger:

- Create a new API Gateway API.
- Define a new resource and method (e.g., POST).
- Connect the method execution to the Lambda function.
- Deploy the API and note the invoke URL.

## Local Testing

For local testing, set the necessary environment variables and run:

`cargo run < test/test_event.json`

## Contributing

Contributions to this project are welcome! Please fork the repository and submit a pull request with your changes or suggestions.
